/* eslint-env node, mocha */

const GitLab = require('gitlab/dist/es5').default;
const expect = require('chai').expect;
const api = new GitLab({
    token: process.env.GITLAB_TOKEN ? process.env.GITLAB_TOKEN : undefined
});

const authorName = process.env.GIT_AUTHOR_NAME ? process.env.GIT_AUTHOR_NAME : undefined;

describe('should create a commit', function () {

    let recentCommits;

    before(function () {
        return api.Commits.all('9856109', { maxPages: 1, perPage: 20 })
            .then(commits => {
                recentCommits = commits;
            });
    });

    it('that adds the ACME challenge', function () {

        const commit = recentCommits.filter((x) => x.author_name === authorName && x.title === 'Automated Let\'s Encrypt renewal: add challenge');

        expect(commit).to.be.an('array').and.have.lengthOf(1);

    });

    it('that removes the ACME challenge', function () {

        const commit = recentCommits.filter((x) => x.author_name === authorName && x.title === 'Automated Let\'s Encrypt renewal: remove challenge');

        expect(commit).to.be.an('array').and.have.lengthOf(1);

    });
});

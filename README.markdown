# gitlab-letsencrypt

Command-line tool to generate a [Let's Encrypt](https://letsencrypt.org) certificate for use with [GitLab Pages](https://pages.gitlab.io/).

## Usage with GitLab CI

1. Create a new project
2. Add a `.gitlab-ci.yml` with the following contents:
   ```yml
   include:
     - https://gitlab.com/leipert-projects/gitlab-letsencrypt/raw/stable/letsencrypt-include.yml

   example.org:
     extends: .issue-certificate
     variables:
      # The custom domains for the Pages project
      LE_DOMAINS: "example.org"
      # The path where the challenge should be added.
      # Please Make sure that this path will be served under
      # http://example.org/.well-known/acme-challenge
      LE_CHALLENGE_PATH: '/public/.well-known/acme-challenge'
      # The full https path to the repository
      LE_GITLAB_REPOSITORY: "https://gitlab.com/user/example-project"
      # LE_EMAIL could be provided here as well, but should be provided as secret variable
      # LE_GITLAB_TOKEN should provide a valid token via secret variables
   ```
3. Create a schedule which runs the script daily. New certificates will only be required if the expires within 30 days

You can add multiple projects as well. For an example please have a look over at: https://gitlab.com/leipert/letsencrypt-websites

### Installation

```sh
npm install -g @leipert/gitlab-letsencrypt
```

### Usage

**Important**: To avoid being rate-limited by Let's Encrypt, do not use the `--production` option until you have tested everything works OK without it.
By default, `gitlab-pages-le` will use [Let's Encrypt's staging environment](https://letsencrypt.org/docs/staging-environment/), which does not issue real certificates but has very generous rate limits.

```sh
gitlab-pages-le \
--email      example@example.com                        `# REQUIRED - Let's Encrypt email address` \
--domain     example.com www.example.com                `# REQUIRED - Domain(s) that the cert will be issued for (separated by spaces)` \
--repository https://gitlab.com/gitlab_user/gitlab_repo `# REQUIRED - Full URL to your GitLab repository` \
--token      ...                                        `# REQUIRED - GitLab personal access token, see https://gitlab.com/profile/personal_access_tokens` \
--production                                            `# OPTIONAL - Obtain a real certificate instead of a dummy one and configure your repository to use it`
--path                                                  `# OPTIONAL - Absolute path in your repository where challenge files should be uploaded`
--jekyll                                                `# OPTIONAL - Upload challenge files with a Jekyll-compatible YAML front matter` \
```

See `gitlab-pages-le --help` for more details.

### Example

```
$ gitlab-pages-le --email example@example.com --token ... --domain example.com www.example.com --repository https://example.com/user/my-repo --production
By using Let's Encrypt, you are agreeing to the TOS at https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf
Uploaded challenge file, waiting for it to be available at http://example.com/.well-known/acme-challenge/35wHSN4YSNjqh5iz5gULxQ4X30cV7vRA_S929uiiNCc
Could not find challenge file. Retrying in 30s...
Could not find challenge file. Retrying in 1m...
Could not find challenge file. Retrying in 2m...
Uploaded challenge file, waiting for it to be available at http://www.example.com/.well-known/acme-challenge/50U-eARIh2OYLQN22oe4lib6_ESnwLPhCMMkBlUY1BI
Could not find challenge file. Retrying in 30s...
Could not find challenge file. Retrying in 1m...
Could not find challenge file. Retrying in 2m...

Success! Your GitLab page has been configured to use an HTTPS certificate obtained from Let's Encrypt.
Try it out: https://example.com https://www.example.com (GitLab might take a few minutes to start using your certificate for the first time)
This certificate expires on Sat Apr 14 2018 03:09:06 GMT+0100 (BST). You will need to run gitlab-pages-le again at some time before this date.
```

## How it works

`gitlab-pages-le` uses the [ACME HTTP Challenge](https://tools.ietf.org/html/draft-ietf-acme-acme-09#section-8.3) to prove ownership of a given set of domains.

1. Requests a challenge from Let's Encrypt using the provided email address for the specified domains. One challenge file is generated per domain
2. Each challenge file is uploaded to your GitLab repository using GitLab's API, which commits to your repository
3. The challenge URL is repeatedly polled until the challenge file is made available. GitLab Pages take a while to update after changes are committed
4. If Let's Encrypt was able to verify the challenge file, a certificate for that domain is issued
5. Each challenge file is removed from your GitLab repository by committing to it through the GitLab API
6. If `--production` was set, your GitLab page is configured to use the issued certificate

Because Let's Encrypt is a fully automated certificate authority, all issued certificates expire in 90 days.
A fresh certificate can be obtained at any time by running `gitlab-pages-le` again, as long as you are within Let's Encrypt's [rate limits](https://letsencrypt.org/docs/rate-limits/).

## Security

`gitlab-pages-le` does not save or log anything to disk.
The GitLab access token is used to upload the challenge file to your repository and to delete it once the challenge is completed.

Even though challenge files are deleted from your repository after a challenge is completed, they are still visible in the repository's commit history.
`gitlab-pages-le` does not rewrite your repository's commit history to hide this.

Challenge files do not have any value to attackers without the RSA keys used to initiate a Let's Encrypt challenge, which are generated on each run of `gitlab-pages-le`.
For more details, refer to the [Integrity of Authorizations sections of the ACME specification](https://tools.ietf.org/html/draft-ietf-acme-acme-09#section-10.2).

## Motivation

Let's Encrypt certificates expire every 90 days - this is by design to take advantage of automated renewals using [ACME](https://tools.ietf.org/html/draft-ietf-acme-acme-01).
However, GitLab does not provide a way to automatically renew certificates, so this process must be done manually.
